package com.toliga.ganjaslave;

import com.toliga.ganjabots.core.AntibanFeature;
import com.toliga.ganjabots.core.AntibanManager;
import com.toliga.ganjabots.core.State;
import com.toliga.ganjabots.core.StateScheduler;
import com.toliga.ganjaslave.states.WaitCommandState;
import org.dreambot.api.methods.Calculations;
import org.dreambot.api.script.AbstractScript;
import org.dreambot.api.script.Category;
import org.dreambot.api.script.ScriptManifest;
import org.dreambot.api.script.listener.MessageListener;
import org.dreambot.api.wrappers.widgets.message.Message;
import org.dreambot.api.wrappers.widgets.message.MessageType;

@ScriptManifest(author = "GanjaSmuggler", category = Category.QUEST, name = "Ganja Slave", description = "Description.", version = 1.0)
public class GanjaSlaveMain extends AbstractScript implements MessageListener {

    private StateScheduler stateScheduler;
    private boolean started = true;
    private State waitCommandState = new WaitCommandState();

    @Override
    public void onStart() {
        super.onStart();

        stateScheduler = new StateScheduler(this, waitCommandState);
    }

    @Override
    public int onLoop() {

        if (started) {
            stateScheduler.executeState(new AntibanManager(this) {
                @Override
                protected AntibanFeature createFeature(String name) {
                    return null;
                }
            });
        }

        return Calculations.random(50, 100);
    }

    @Override
    public void onExit() {
        super.onExit();
        GlobalSettings.CLEAR();
    }

    @Override
    public void onGameMessage(Message message) {

    }

    @Override
    public void onPlayerMessage(Message message) {
    }

    @Override
    public void onTradeMessage(Message message) {

    }

    @Override
    public void onPrivateInMessage(Message message) {
        AbstractScript.log("out");
        if (getFriends().haveFriend(message.getUsername())) {
            AbstractScript.log("in");
            AbstractScript.log(message.getMessage());
            GlobalSettings.MESSAGE = message;
            if (message.getMessage().contains(" ")) {
                GlobalSettings.COMMAND = message.getMessage().split(" ")[0];
                GlobalSettings.ITEM = message.getMessage().split(" ")[1];
            } else {
                GlobalSettings.COMMAND = message.getMessage();
            }
        }
    }

    @Override
    public void onPrivateOutMessage(Message message) {

    }
}
