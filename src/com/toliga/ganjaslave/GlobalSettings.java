package com.toliga.ganjaslave;

import org.dreambot.api.methods.map.Tile;
import org.dreambot.api.wrappers.widgets.message.Message;

public class GlobalSettings {
    public static boolean DEBUG = true;

    public static String ITEM = null;
    public static String COMMAND = null;
    public static Message MESSAGE = null;
    public static Tile SOURCE_TILE = null;

    public static void CLEAR() {
        ITEM = null;
        COMMAND = null;
        MESSAGE = null;
        SOURCE_TILE = null;
    }
}
