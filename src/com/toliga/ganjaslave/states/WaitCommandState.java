package com.toliga.ganjaslave.states;

import com.toliga.ganjabots.core.AntibanManager;
import com.toliga.ganjabots.core.State;
import com.toliga.ganjaslave.GlobalSettings;
import org.dreambot.api.script.AbstractScript;
import org.dreambot.api.wrappers.items.GroundItem;

public class WaitCommandState implements State {

    private State nextState;

    @Override
    public boolean execute(AbstractScript context, AntibanManager antibanManager) {
        AbstractScript.log("WAIT_COMMAND");

        // command objectName

        String item = GlobalSettings.ITEM;

        if (context.getInventory().isFull()) {
            nextState = new WalkToBankState();
            return true;
        }

        if (GlobalSettings.COMMAND != null) {
            AbstractScript.log(GlobalSettings.COMMAND);
            AbstractScript.log(GlobalSettings.MESSAGE.getUsername());

            if (GlobalSettings.COMMAND.equalsIgnoreCase("bank")) {
                if (item != null) {
                    GroundItem obj = context.getGroundItems().closest(item);
                    obj.interact();
                }
            }

            if (GlobalSettings.COMMAND.equalsIgnoreCase("come")) {
                nextState = new WalkFromBankState();
                return true;
            }
        }

        return false;
    }

    @Override
    public State next() {
        return nextState;
    }
}
