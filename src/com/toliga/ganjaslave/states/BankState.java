package com.toliga.ganjaslave.states;

import com.toliga.ganjabots.core.AntibanManager;
import com.toliga.ganjabots.core.State;
import com.toliga.ganjabots.core.Utilities;
import com.toliga.ganjaslave.GlobalSettings;
import org.dreambot.api.methods.tabs.Tab;
import org.dreambot.api.script.AbstractScript;

public class BankState implements State {

    @Override
    public boolean execute(AbstractScript context, AntibanManager antibanManager) {
        if (GlobalSettings.DEBUG) AbstractScript.log("BANK");
        Utilities.OpenTab(context, Tab.INVENTORY);

        if (!context.getBank().isOpen() && context.getBank().open()) {
            AbstractScript.sleepUntil(() -> context.getBank().isOpen(), 9000);
        }

        if (context.getBank().isOpen()) {
            context.getBank().depositAll(GlobalSettings.ITEM);
            AbstractScript.sleepUntil(() -> context.getInventory().count(GlobalSettings.ITEM) == 0, 10000);
        }

        if (context.getInventory().count(GlobalSettings.ITEM) == 0) {
            if (context.getBank().close()) {
                AbstractScript.sleepUntil(() -> !context.getBank().isOpen(), 8000);
                return true;
            }
        }

        return false;
    }

    @Override
    public State next() {
        return new WalkFromBankState();
    }
}
